
<!-- README.md is generated from README.Rmd. Please edit that file -->

## shinylib

Une librairie d’utilitaire shiny.

## Objectifs

  - Faciliter le travail en réutilisant du code existant (plus de
    **copier/coller**)
    
      - Utiliser des snippets de code
      - Créer des modules shiny

  - Tester les modules sous différentes forme d’app finale (normal,
    dashboard, …)

  - Utiliser des sous-modules et des formes plus complexes de structure

### 1\) Code Snippet

  - 1.1 Dans Tools \> Global … \> Code \> Edit Snippets , ajouter les
    snippets suivant si besoin :

<!-- end list -->

``` r
snippet module
    ${1:name}ui <- function(id){
        ns <- NS(id)
        tagList(
        
            )
        }

    ${1:name} <- function(input, output, session){
        ns <- session\$ns
    }
    
    # Copy in UI
    ${1:name}ui("${1:name}ui")
    
    # Copy in server
    callModule(${1:name}, "${1:name}ui")
    
snippet observe_event
    observeEvent( ${1:event} , {
    
    })
    
snippet with_progress
    withProgress( message = "${1:message}" , {
    
    })

snippet ui_snippet
    library(shiny)
    
    ui <- function(){
        fluidPage(
        
        )
    }
    
    server <- function(input, output, session){
    
    }
    
    shinyApp(ui, server)
```

  - 1.2 Tester le `ui_snippet` pour créer une app de base.

  - 1.3 Créer les scripts `ui_fun.R`, `server_fun.R`, `run_app.R` comme
    vu au 1er exercice.

### 2\) Modules

Les modules sont l’équivalent des fonctions R dans Shiny. On peut les
importer et les exporter dans des applications différentes.

  - 2.1 Créer un module `mod_scatterplot.R` à l’aide du snippet
    correspondant.

  - 2.2 Ce module va afficher un scatterplot de `iris` en permettant de
    sélectionner les variables numériques voulues

  - 2.3 Tester le module en modifiant les scripts ui.R et server.R

  - 2.4 Modifier le module pour qu’il prenne un argument `.data` de type
    data.frame et trace le scatterplot correspondant.

  - 2.5 Créer un nouveau module `mod_scatterplot_aes.R` qui permette à
    l’utilisateur de choisir les options “shape”, “size” et “alpha” du
    scatterplot.

### 3\) Projet : librairie shiny

  - 3.1 Créer de nouvelles fonctions permettant de créer l’app
    précédente sous forme de dashboard.

  - 3.2 Créer un projet comportant les briques à suivre.
    
    *Pour plus de clarté l’architecture suivante est proposée*
    
      - Un module pour importer les données (à l’aide d’une fonction
        dans un script à part)
    
      - Un module pour vérifier la qualité des données
    
      - Un module pour filtrer les données
    
      - Un module pour visualiser des statistiques descriptives
    
      - Un sous-module pour visualiser un boxplot
    
      - Un sous-module pour visualiser un barplot
    
      - Un sous-module pour visualiser une heatmap
    
      - Un sous-module pour visualiser un chordiagram

  - 3.3 Compiler et faire les checks et tests nécessaires.

### 4\) Travail sur des projets actuels

Selon le timing, définir une architecture pour créer des packages de
fonction R et de module shiny utiles.
